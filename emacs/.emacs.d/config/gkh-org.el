(setq org-directory
      (let ((sys (system-name)))
        (cond
         ((string-prefix-p "upsy-ae04-2240" sys) "~/org/")
         ((string-prefix-p "APC30425N" sys) "c:/Users/gross20/org/"))))

(setq gkh/journal-file
        (let ((sys (system-name)))
          (cond
           ((string-prefix-p "upsy-ae04-2240" sys) "Privat.org")
           ((string-prefix-p "APC30425N" sys) "Arbeit.org"))))

(setq org-default-notes-file (expand-file-name (concat org-directory gkh/journal-file)))

;; Cosmetics


(setq org-hide-emphasis-markers t
      org-hide-leading-stars t
      org-startup-indented t
      org-pretty-entities nil)

(setq org-special-ctrl-a/e t)

;; Motion
;; :LOGBOOK:
;; - Note taken on [2024-07-12 Fr 08:50] \\
;;   With this settings, I can use =C-c C-j= and get a familar interface to
;;   jump to a specific heading in the given org-file.
;; :END:


(setq org-goto-interface 'outline-path-completion
      org-outline-path-complete-in-steps nil)

;; Archive


(setq org-archive-save-context-info '(time))

;; Agenda


(setq org-agenda-files (list gkh/journal-file)
      org-agenda-follow-indirect t)

(cond ((string-prefix-p "upsy-ae04-2240" system-name)
       (add-to-list 'org-agenda-files "Mobile.org" t)))

(add-hook 'org-agenda-finalize-hook #'hl-line-mode)

(setq org-agenda-custom-commands
      '(
        ("u" "Unscheduled"
         todo ""
         ((org-agenda-todo-ignore-scheduled 'all)
          (org-agenda-todo-ignore-deadlines 'all)))
        ("W" "Weekly Review"
         agenda ""
         ((org-agenda-span 'week)
          (org-agenda-start-on-weekday 0)
          (org-agenda-start-with-log-mode t)
          (org-agenda-skip-function
           '(org-agenda-skip-entry-if 'nottodo 'done))))))

(setq org-deadline-warning-days 6)

(defun org-agenda-edit-headline ()
  "Edit the headline for the current headline."
  (interactive)
  (org-agenda-check-no-diary)
  (let* ((hdmarker (or (org-get-at-bol 'org-hd-marker)
                       (org-agenda-error)))
         (buffer (marker-buffer hdmarker))
         (pos (marker-position hdmarker))
         (inhibit-read-only t)
         ) ;; newhead
    (org-with-remote-undo buffer
      (with-current-buffer buffer
        (widen)
        (goto-char pos)
        (org-fold-show-context 'agenda)
        (call-interactively 'org-edit-headline)))))

;; Capture


(setq denote-org-capture-specifiers "%?")

(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "" "Offene Aufgaben")
         "* TODO %?%^{CATEGORY}p\n:PROPERTIES:\n:ID: %<%Y%m%dT%H%M%S>\n:CAPTURE_TIME: %U\n:END:\n"
         :prepend t)
        ("n" "Notiz" entry (file+olp+datetree "")
         ;; Timeformat: 2024-07-05 Freitag, 08:59h
         "* %<%Y-%m-%d %A, %H:%Mh> %^{CATEGORY}p\n:PROPERTIES:\n:ID: %<%Y%m%dT%H%M%S>\n:CAPTURE_TIME: %U\n:END:\n%?"
         :before-finalize gkh/org-end-time)
        ("z" "Zettel" plain
                   (file denote-last-path)
                   #'denote-org-capture
                   :no-save t
                   :immediate-finish nil
                   :kill-buffer t
                   :jump-to-captured t)
        ("wl" "Link (web)" entry (file+headline "~/Journal/weblinks.txt" "Links")
         "* %a\n:PROPERTIES:\n:CAPTURE_TIME: [%<%Y-%m-%d %a>]\n:END:\n"
         :immediate-finish t :kill-buffer t)))

  ;; (with-eval-after-load 'org-capture
  ;;   (add-to-list 'org-capture-templates
  ;;                '("z" "Zettel" plain
  ;;                  (file denote-last-path)
  ;;                  #'denote-org-capture
  ;;                  :no-save t
  ;;                  :immediate-finish nil
  ;;                  :kill-buffer t
  ;;                  :jump-to-captured t)
  ;;                t ))


(defun gkh/org-capture-time ()
  (let ((ts (format-time-string "[%Y-%m-%d %a %H:%M]")))
    (org-entry-put nil "END_TIME" ts)))

(setq org-id-method 'ts
      org-id-ts-format "%Y%m%dT%H%M%S" ;; also used in capture template
      org-id-link-to-org-use-id t)

;; Attach


(require 'org-attach)

(defun gkh/org-attach-id-ts-folder-format (id)
  "Translate an ID based on a timestamp to a folder-path.
Useful way of translation if ID is generated based on ISO8601
timestamp.  Splits the attachment folder hierarchy into
year, the rest."
  (format "%s/%s"
          (substring id 0 4)
          (substring id 4)))

(add-to-list 'org-attach-id-to-path-function-list 'gkh/org-attach-id-ts-folder-format)

(setq org-attach-method "lns"
      ;; org-attach-store-link-p "attached"
      org-attach-use-inheritance t
      org-attach-dir-relative t
      org-attach-id-dir "."
      org-attach-preferred-new-method 'ask
      org-attach-auto-tag "attach")

;; Refile


(setq org-refile-targets '((org-agenda-files :maxlevel . 3))
      org-refile-use-outline-path t)

;; Tempo


(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("r" . "src R"))

;; Export


(setq org-export-default-language "de")
;(setq org-odt-preferred-output-format "docx")
(setq user-full-name "Jonas Großekathöfer")

;; Keybindings


(setq org-M-RET-may-split-line '((default . nil))
      org-insert-heading-respect-content t)

(let ((map global-map))
  (define-key map (kbd "C-c c") #'org-capture)
  (define-key map (kbd "C-c a") #'org-agenda)
  (define-key map (kbd "C-c l") #'org-store-link))

(with-eval-after-load 'org-agenda
  (let ((map org-agenda-mode-map))
    (define-key map (kbd "C-c C-x h") #'org-agenda-edit-headline)))

(let ((map org-mode-map))
    (define-key map (kbd "C-c C-x h") #'org-edit-headline))

(let ((map org-src-mode-map))
  (define-key map (kbd "C-c C-c") #'org-edit-src-exit))

;; Custom functions


;; eigene Funktionen, does not work for properties
(defun gkh/org-collect-keywords-url ()
  "Get url from org keyword ~#+url~."
  (cdr (car (org-collect-keywords
             '("url")))))

;; TODO: Make it work in Agenda view
(defun gkh/org-entry-url-browse-url ()
  "If multiple URL found, open dispatch to choose from."
  (interactive)
  (let ((url (org-entry-get nil "url")))
    (browse-url url)))

;; Babel


(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (shell . t)))

(if (eq system-type 'windows-nt)  
    (setq org-babel-R-command (concat
                               (convert-standard-filename "C:/Program Files/R/bin/x64/")
                               " --slave --no-save")))

;; ~org-bookmark-heading~


(require 'org-bookmark-heading)
