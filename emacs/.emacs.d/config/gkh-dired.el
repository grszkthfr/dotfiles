(setq dired-dwim-target t
      dired-auto-revert-buffer t)

(add-hook 'dired-mode-hook
          (lambda ()
            (dired-hide-details-mode)
            (denote-dired-mode)))
