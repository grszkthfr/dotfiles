;; Cosmetic settings


(tool-bar-mode -1)
(load-theme 'modus-operandi)

(set-face-attribute 'default nil
                      :family "Cascadia Code" :height 110)

(setq ring-bell-function 'ignore)
(setq visible-bell t)

;; Functional settings


;; Safe local variables
(add-to-list 'safe-local-variable-values '(eval add-hook 'before-save-hook 'time-stamp))

;; Use y or n
(fset #'yes-or-no-p #'y-or-n-p)

;; Move through windows with Ctrl-<arrow keys>
(windmove-default-keybindings 'ctrl)

(setq backup-by-copying t
      delete-old-versions t
      kept-new-versions 10
      kept-old-versions 5
      version-control t)

;; Remember recently opened files
(recentf-mode 1)

(add-to-list 'recentf-exclude
             (recentf-expand-file-name no-littering-var-directory))

(add-to-list 'recentf-exclude
             (recentf-expand-file-name no-littering-etc-directory))
(add-hook 'find-file-hook 'recentf-save-list)

(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 250)

(let ((map global-map))
  (define-key map (kbd "C-c r") #'recentf-open-files))

;; Revert buffer, if changed on disk
(global-auto-revert-mode 1)

;; Remember last curor position in file  
(save-place-mode 1)

;; Remember windows layouts
(winner-mode 1)

;; Save minibuffer/kill-ring/register histories
(setq savehist-additional-variables '(register-alist kill-ring))
(savehist-mode 1)

;; ;; Add Git/ to 'exec-path' to use unix tools like diff or find
 (push "C:\\Program Files\\Git\\usr\\bin" exec-path)

 ;; This breaks the path....
 ;; (setenv "PATH"
 ;;         (concat
 ;;          "C:\\Program Files\\Git\\usr\\bin;"
 ;;          (getenv "PATH")))

;; Save 'bookmark-file' immediatly when set.
(setq bookmark-save-flag 1)

;; Let sentence end with a single space
(setq sentence-end-double-space nil)

;; Let proced auto-update processes by default
(defun proced-settings ()
  (proced-toggle-auto-update t))

(add-hook 'proced-mode-hook 'proced-settings)

;; Isearch
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-settings.el
;; :END:


(setq isearch-lazy-count t)
(setq lazy-count-prefix-format "(%s/%s) ")
(setq lazy-count-suffix-format nil)

;; Wgrep
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-settings.el
;; :END:


(require 'wgrep)
