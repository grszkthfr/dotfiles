(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(ledger-mode eat org-bookmark-heading wgrep magit no-littering denote consult-recoll ess consult-notes consult vertico quelpa orderless elpa-mirror))
 '(safe-local-variable-values
   '((org-confirm-babel-evaluate)
     (eval add-hook 'before-save-hook 'time-stamp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
