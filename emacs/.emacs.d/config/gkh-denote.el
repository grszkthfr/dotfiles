(require 'denote)

(add-hook 'dired-mode-hook
          (lambda ()
            (dired-hide-details-mode)
            (denote-dired-mode)))

(setq denote-directory (expand-file-name (concat org-directory "Zettelkasten/")))
(setq denote-known-keywords nil)
(setq denote-org-front-matter
      "#+title: %s

#+time_created:  %s
#+time_modified: 

#+filetags:      %s
#+property:      ID %s

\n\n")
