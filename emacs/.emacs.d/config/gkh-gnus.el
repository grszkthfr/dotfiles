;; Sending Mails


;; Sending mails
(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-smtp-server "smtp.purelymail.com"
      smtpmail-stream-type 'starttls
      smtpmail-smtp-service 587)

(setq mail-user-agent 'gnus-user-agent) ; only respected with GNUS open?!
(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-smtp-server "smtp.purelymail.com"
      smtpmail-stream-type 'starttls
      smtpmail-smtp-service 587)

;; forwarding with "Fwd:"
(setq message-make-forward-subject-function 'message-forward-subject-fwd)

;; GNUS


;; Gnus hardcodes newsrc.eld to be based on gnus-startup-file.
(setq gnus-home-directory (concat no-littering-var-directory "gnus/")
      gnus-directory (concat no-littering-var-directory "gnus/news/"))

(defun gkh/gnus-group-list-subscribed-groups ()
  "List all subscribed groups with or without unread messages"
  (interactive)
  (gnus-group-list-all-groups 5))

;; (define-key gnus-group-group-mode-map
;;   (kbd "o") 'gkh/gnus-group-list-subscribed-groups)

(setq gnus-save-newsrc-file nil)

(setq gnus-select-method
      '(nnimap "Email"
               (nnimap-address "imap.purelymail.com")
               (nnimap-server-port 993)
               (nnimap-stream ssl)
               (nnmail-expiry-target "nnimap:Trash")))

;; Unsure, what this is able to provide...
(add-to-list 'gnus-secondary-select-methods '(nntp "news.gnus.org"))
;; (add-to-list 'gnus-secondary-select-methods '(nntp "news.gmane.io"))

;; (setq gnus-parameters '(("INBOX" (display . 100))
;;                         ("Archive" (display . 100))
;;                         ("Sent" (display . 100))))

(setq gnus-message-archive-group '("nnimap:Sent")) ; https://stackoverflow.com/a/16155985

(setq gnus-posting-styles
      '((".*" ; Matches all groups of messages
         ("X-Message-SMTP-Method" "smtp smtp.purelymail.com 587 jonas@grszkth.fr"))))

(setq mail-user-agent 'gnus-user-agent) ; only respected with GNUS open?!

(setq gnus-permanently-visible-groups ".*"
      ;; gnus-fetch-old-headers t ;; when a new followup arrives in a thread, see the previous messages from that thread, too.
      gnus-thread-sort-functions '(gnus-thread-sort-by-most-recent-date)
      gnus-article-sort-functions '(not gnus-article-sort-by-date)) ;; newst at the top

(setq gnus-summary-line-format "%U%R%d %-20,20n %B%-80,80S\n"
      gnus-sum-thread-tree-vertical "│"
      gnus-sum-thread-tree-root ""
      gnus-sum-thread-tree-false-root ""
      gnus-sum-thread-tree-indent " "
      gnus-sum-thread-tree-single-indent ""
      gnus-sum-thread-tree-leaf-with-other "├► "
      gnus-sum-thread-tree-single-leaf "╰► ")

(setq gnus-article-date-headers 'combined-local-lapsed)
