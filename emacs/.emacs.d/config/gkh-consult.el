(defun gkh/project-find-org-files (directory &optional include-all)
    "Return list of all org-files for the project under the given DIRECTORY.

  TODO If INCLUDE-ALL is non-nil, or with prefix argument when called,
  include all files under the project root, except for VCS
  directories listed in `vc-directory-exclusion-list'. "
    (let* ((pr (project-current nil directory))
           (all-files (project-files pr))
           ;; filter all org-files from project files
           (all-org (seq-filter (lambda (x) (string-match ".org$" x)) all-files)))
    ;; remove interlock files (see https://www.emacswiki.org/emacs/AutoSave#h5o-3)
    (seq-remove (lambda (x) (string-match ".#" x)) all-org))
    )

(require 'consult)
(require 'consult-org)

(let ((map global-map))
  (define-key map (kbd "C-x b") #'consult-buffer)
  (define-key map (kbd "C-x r b") #'consult-bookmark)
  (define-key map (kbd "M-g m") #'consult-mark)
  (define-key map (kbd "M-g k") #'consult-global-mark)
  (define-key map (kbd "M-y") #'consult-yank-pop))

;; Notes integration
(require 'consult-notes)

(if (eq system-type 'windows-nt)
    ;; then
    (setq consult-notes-org-headings-files
          (append org-agenda-files
                  ;; This is not working on Windows, problems with find
                  ;; (gkh/project-find-org-files "~/Dotfiles")
                  ;; Workaround
                  '("~/Dotfiles/emacs/README.org")))
  ;; else
  (setq consult-notes-org-headings-files
    (append org-agenda-files
            (gkh/project-find-org-files "~/Dotfiles"))))


(consult-notes-org-headings-mode)
(consult-notes-denote-mode)

(add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

;; Keybindings
(let ((map global-map))
  (define-key map (kbd "C-c n n") #'consult-notes))
