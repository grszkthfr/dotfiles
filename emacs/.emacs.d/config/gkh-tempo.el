(require 'tempo)
;; (setq tempo-interactive t)

(if (string-equal system-name "APC30425N")
    (tempo-define-template "org-list-asd-ags"
                           '("+ AGs" n
                             "  + AGDH | AG Datenhaltung" n
                             "  + AGDQ | AG Datenqualität" n
                             "  + *AGDS | AG Datenschutz*" n
                             "  + AGER | AG Erhebungsinhalte" n
                             "  + AGPA | AG Prozessausgestaltung" n
                             "  + *AGID | AG Identitätsmanagement*" n
                             "  + AGRN | AG Rechtliche Prüfung und Normierung" n
                             "  + AGSE | AG Schnittstelle" n
                             "  + +AGSH | AG Stakeholder+" n
                             "  + *AGTD | AG Testdaten*" n
                             "  + *AGSL | AG Schulung*"
                             )
                           nil
                           "Inserts a list with all ASD AGs."))
