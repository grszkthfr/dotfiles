;; early-init.el
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/early-init.el
;; :END:


(set-language-environment "UTF-8")

;; (push '(tool-bar-lines . 0) default-frame-alist)
;; (push '(menu-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

(setq gc-cons-threshold (* 50 1000 1000))

(when (fboundp 'startup-redirect-eln-cache)
  (startup-redirect-eln-cache
   (convert-standard-filename
    (expand-file-name  (concat "data/" system-name "/eln-cache/") user-emacs-directory))))
