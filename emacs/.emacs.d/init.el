;; Packages
;; :LOGBOOK:
;; - Note taken on [2024-07-12 Fr 08:57] \\
;;   Put gkh-elpa at the beginning of the ~package-archvives~-list to
;;   prefer - if available - packages from local gkh-elpa. This might
;;   prevents accidental updates. I am not sure, how this interferes with
;;   installing latest packages via ~package-install~, so this might need
;;   to get reverted in the future.
;; - Note taken on [2024-05-20 Mon 09:05] \\
;;   Now everything is installed with MELPA... mhmm...
;; - Note taken on [2024-05-20 Mon 09:04] \\
;;   Can get elpa-mirror only to work with melpa. Not with ELPA. I think
;;   because ~no-littering~, ~elpa-mirror~ and ~consult-notes~ are not on ELPA,
;;   and with "manual" installation I don't get it to work...
;; - Note taken on [2024-05-18 Sat 12:42] \\
;;   To load ~magit~, installed via ELPA, for my local machine run
;;   =(package-initialize)=
;; :END:


;; Set custom-file, even when ~no-littering~ is not installed.
(setq custom-file (convert-standard-filename
                   (expand-file-name "config/gkh-custom.el" user-emacs-directory)))

;; ~package-selected-packages~ is ~nil~ until =gkh-custom.el= is
;; loaded on a fresh system (as ~no-littering~ is not yet installed)
(load-file custom-file)

(require 'package)

;; Local package archvie
(add-to-list 'package-archives
             '("gkh-elpa" . "~/.emacs.d/gkh-elpa/"))

;; Install packages specified in gkh-custom.el
(package-install-selected-packages t)

;; Littering
;; :LOGBOOK:
;; - Note taken on [2024-04-19 Fri 08:48] \\
;;   The =config= directory stores permanent and /hand crafted/ configuration
;;   files, which can be also referred toin a more traditionally way as
;;   "etc". One exception to this description is GNUS ~.newsrc.eld~, which is
;;   generated and updated automatically. The =data= directory (traditionally
;;   the "var/" directory) stores automatically generated files, such as
;;   bookmarks, backups, histories, ... . Note that only some of these
;;   files can be considered permanent and worth backing up (e. g. the
;;   ~bookmarks-default-file~).
;; :END:


(setq no-littering-var-directory
      (convert-standard-filename
       (expand-file-name (concat "data/" system-name "/") user-emacs-directory)))

(setq no-littering-etc-directory
      (convert-standard-filename
       (expand-file-name "config/" user-emacs-directory)))

;; Please do not litter
(require 'no-littering)

(no-littering-theme-backups)

;; Put litter in trash
(setq-default delete-by-moving-to-trash t)

;; Cosmetics and Basics
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-settings.el
;; :END:
;; :LOGBOOK:
;; - Note taken on [2024-04-19 Fri 07:17] \\
;;   TODO Split functional settings from cosmetic settings?
;; - Note taken on [2024-04-16 Tue 21:25] \\
;;   TODO Code sortieren...
;; - Note taken on [2024-03-28 Thu 21:15] \\
;;   TODO git to exec-path only on win machines
;; :END:


(load-file (expand-file-name "gkh-settings.el" no-littering-etc-directory))

;; Completion
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-completion.el
;; :END:
;; :LOGBOOK:
;; - Note taken on [2024-04-19 Fri 07:21] \\
;;   TODO understand ~completion-cycle-threshold~ and ~tab-always-indent~
;; :END:


(load-file (expand-file-name "gkh-completion.el" no-littering-etc-directory))

;; Dired
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-dired.el
;; :END:


(load-file (expand-file-name "gkh-dired.el" no-littering-etc-directory))

;; Project
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-project.el
;; :END:


(load-file (expand-file-name "gkh-project.el" no-littering-etc-directory))

;; Eshell / Eat
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-eshell.el
;; :END:


(load-file (expand-file-name "gkh-eshell.el" no-littering-etc-directory))

;; Orgmode
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-org.el
;; :END:


(load-file (expand-file-name "gkh-org.el" no-littering-etc-directory))

;; Denote
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-denote.el
;; :END:


(load-file (expand-file-name "gkh-denote.el" no-littering-etc-directory))

;; Consult
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-consult.el
;; :END:
;; :LOGBOOK:
;; - Note taken on [2024-08-02 Fri 10:44] \\
;;   Denote keybindings removed, I didn't use those...
;; - Note taken on [2024-04-21 Sun 21:02] \\
;;   TODO For ~INCLUDE-ALL~ (see below), take the relevant bits from [[file:/snap/emacs/current/usr/share/emacs/29.3/lisp/progmodes/project.el.gz::defun project-find-file-in (suggested-filename dirs project &optional include-all][here]].
;; :END:


(load-file (expand-file-name "gkh-consult.el" no-littering-etc-directory))

;; Tempo
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-tempo.el
;; :END:


(load-file (expand-file-name "gkh-tempo.el" no-littering-etc-directory))

;; ESS
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-ess.el
;; :END:


;; (load "ess-autoloads")

;; Ediff
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-ediff.el
;; :END:


(load-file (expand-file-name "gkh-ediff.el" no-littering-etc-directory))

;; Mail / GNUS
;; :PROPERTIES:
;; :header-args:emacs-lisp+: :tangle .emacs.d/config/gkh-gnus.el
;; :END:


(setq gnus-init-file (concat no-littering-etc-directory "gkh-gnus.el"))
