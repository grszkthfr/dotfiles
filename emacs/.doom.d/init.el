;;; init.el -*- lexical-binding: t; -*-
(doom! :input

       :completion
       company           ; the ultimate code completion backend
       (vertico
        +icons)

       :ui
       doom              ; what makes DOOM look the way it does
       doom-dashboard    ; a nifty splash screen for Emacs
       doom-quit         ; DOOM quit-message prompts when you quit Emacs
       fill-column       ; a `fill-column' indicator
       hl-todo           ; highlight TODO/FIXME/NOTE/DEPRECATED/HACK/REVIEW
       hydra
       indent-guides     ; highlighted indent columns
       minimap           ; show a map of the code on the side
       modeline          ; snazzy, Atom-inspired modeline, plus API
       nav-flash         ; blink the current line after jumping
       ophints           ; highlight the region an operation acts on
       (popup +defaults) ; tame sudden yet inevitable temporary windows
       (ligatures
        +extra)       ; replace bits of code with pretty symbols
       treemacs          ; a project drawer, like neotree but cooler
       unicode           ; extended unicode support for various languages
       (emoji +unicode)  ; 🚀
       vc-gutter         ; vcs diff in the fringe
       vi-tilde-fringe   ; fringe tildes to mark beyond EOB
       window-select     ; visually switch windows
       workspaces        ; tab emulation, persistence & separate workspaces
       zen               ; distraction-free coding or writing

       :editor
       (evil +everywhere); come to the dark side, we have cookies
       file-templates    ; auto-snippets for empty files
       fold              ; (nigh) universal code folding
       format  ; automated prettiness
       multiple-cursors  ; editing in many places at once
       snippets          ; my elves. They type so I don't have to
       word-wrap         ; soft wrapping with language-aware indent

       :emacs
       (dired             ; making dired pretty [functional]
        +icons)
       electric          ; smarter, keyword-based electric-indent
       ibuffer           ; interactive buffer management
       undo              ; persistent, smarter undo for your inevitable mistakes
       vc                ; version-control and Emacs, sitting in a tree

       :term
       eshell            ; a consistent, cross-platform shell (WIP)

       :checkers
       syntax            ; tasing you for every semicolon you forget
       (spell
        +everywhere)     ; tasing you for misspelling mispelling

       :tools
;       biblio
       editorconfig      ; let someone else argue about tabs vs spaces
       (eval +overlay)   ; run code, run (also, repls)
       lookup            ; navigate your code and its documentation
       lsp
       magit             ; a git porcelain for Emacs
       pdf               ; pdf enhancements
       rgb               ; creating color strings

       :os
       tty                 ; improve the terminal Emacs experience

       :lang
       sh                ; she sells {ba,z,fi}sh shells on the C xor
       emacs-lisp        ; drown in parentheses
       (ess +lsp)        ; emacs speaks statistics
       (julia +lsp)      ; a better, faster MATLAB
       (python +lsp)     ; beautiful is better than ugly
       javascript        ; all(hope(abandon(ye(who(enter(here))))))
       web               ; the tubes
       data              ; config/data formats
       latex             ; writing papers in Emacs has never been so fun
       yaml              ; JSON, but readable
       markdown          ; writing docs for people to ignore
       (org              ; organize your plain life in plain text
        +roam
        +dragndrop       ; drag & drop files/images into org buffers
        +hugo            ; use Emacs for hugo blogging
        +pandoc          ; export-with-pandoc support
        +present)        ; using org-mode for presentations

       :email

       :app
       everywhere

       :config
       literate
       (default +bindings +smartparens))

(setq +literate-config-file "~/.dotfiles/emacs/.doom.d/README.org")
