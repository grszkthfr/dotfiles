# Myself


export EMAIL="mail@grszkth.fr"
export NAME="Jonas D. Großekathöfer"
export SMTPSERVER="smtp.purelymail.com"

# Editor - Emacs

# What are the default editors?


export ALTERNATE_EDITOR=""                      # ???
export EDITOR="emacsclient -t -a ''"            # $EDITOR opens in terminal
export VISUAL="emacsclient -c -a emacs"         # $VISUAL opens in GUI mode

# Path

# Where to find executables.


export PATH="$HOME/.local/bin:$HOME/.local/bin/rM/:$HOME/.emacs.d/bin:/$HOME/.texlive/2022/bin/x86_64-linux:$PATH"

# Ledger


export LEDGER_FILE=$HOME/Documents/Finanzen/2024.ledger

# R

# Where should =R= install its libraries? Seems to be the only place understood from
# RStudio, the terminal and Emacs. [[ https://rstats.wtf/][This]] is a great resource understanding /R/'s
# internals.


export R_LIBS_USER="$HOME/.local/lib/R%p-library/%v/"



# Control which version of =R= Rstudio should use.


export RSTUDIO_WHICH_R="/usr/bin/R"

# .bashrc

# Load =.bashrc= to get login environment.


[ -f ~/.bashrc ] && . ~/.bashrc
