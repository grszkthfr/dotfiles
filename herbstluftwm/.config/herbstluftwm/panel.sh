#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# for m in $(polybar --list-monitors | cut -d":" -f1); do
#     WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar --reload mainbar-i3 &
# done

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload herbst &
  done
else
  polybar --reload herbst &
fi

echo "Bars launched..."
